import { IUserRepository } from ".";

export interface IRepositories {
    userRepository: IUserRepository;
}
